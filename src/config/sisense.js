
const settings = {
    "server": "http://takashi.sisense.com",
    "dashboardTitlePrefix": "Sample",
    "applicationDivId": "sisenseApp",
    "widgetTypesToExclude": ["indicator", "richtexteditor"]
}

export const sisenseSettings = settings;
